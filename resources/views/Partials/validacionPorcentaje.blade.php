<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
    <script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>
	 <script type="text/javascript">
           
      function validacionPorcentaje(){
        var porcentaje = document.getElementById('porcentaje').value;
        console.log(porcentaje);

        
         if(porcentaje < 1){
          swal("Porcentaje no correcto minimo 1 %", "click en ok para cerrar!", "warning");
               
               return false;
        }
        else if(porcentaje > 100){
          swal("Porcentaje no correcto maximo 100 %", "click en ok para cerrar!", "warning");
               
               return false;
        }
      }

     function soloNumeros(e){
          var key = window.Event ? e.which : e.keyCode
          return (key >= 48 && key <= 57)
      } 
    </script>
</body>
</html>