<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Autor;
use Faker\Generator as Faker;

$factory->define(Autor::class, function (Faker $faker) {
    return [
        'sigi_nombres' => strtoupper($faker->firstNameMale) ,
        'sigi_apellidos' => strtoupper($faker->lastName),
        'sigi_tipo_dni' => strtoupper($faker->randomElement(['C', 'P'])),
          'sigi_dni'=> $faker->numerify('##########'),
          'sigi_sexo' => strtoupper($faker->randomElement(['H', 'M'])),
          'sigi_fecha_autor' => $faker->date($format = 'Y/m/d', $max = 'now'),
           'sigi_email'=> strtoupper($faker->email) 


    ];
});
