<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        if(Auth::user()->role->sigi_nombre == 'ADMINISTRADOR'){
                $role = Role::all();
                $leer = $role;
                return view('roles.index',compact('leer'));    
        }else{
            return "No eres Administrador";
        }
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = [
            'sigi_nombre' => 'required | min:3',
          
        ];
      
      $this->validate($request,$datos);
        $datosRol=request()->except('_token');
        $datosRol['sigi_nombre']= strtoupper($request->get('sigi_nombre'));
      

        
         
        Role::insert($datosRol);
       
        return redirect('/rol')->with([
            'Mensaje' => 'Rol Agregado'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $actualizar =  Role::findOrFail($id);
        return view('roles.edit',compact('actualizar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
       $datos = [
            'sigi_nombre' => 'required | min:3',
            'sigi_estado' => 'required'
        ];
      
      $this->validate($request,$datos);


       $datosRol = request()->except('_token','_method'); 
       $datosRol['sigi_nombre']= strtoupper($request->get('sigi_nombre'));
        
       Role::whereId($id)->update($datosRol);
       
       return redirect("rol")->with([
        'Mensaje' => 'Rol actualizado'
       ]);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::whereId($id)->delete();
       
        return redirect('/rol')->with([
        'Mensaje' => 'Rol Eliminado',
         ]);
    }
}
