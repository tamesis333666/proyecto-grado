@extends('layouts.plantilla')
@section('styles')


@endsection    
 
@section('content')
@include("partials.validacionLibro")
@include("partials.validacionISBN")
		
		<div class="row">
			    <div class="col-lg-12 ">
			        <div class="panel">
			            <div class="panel-heading">
			                <h3 class="panel-title">Editar Informacion del libro</h3>
			            </div>
			
			
			       
			            <!--===================================================-->
			            <form  action="{{ route('libro.update', $actualizar->id) }}" onsubmit="return validacionLibro()" method="post" class="panel-body form-horizontal form-padding">
			            	@csrf
			            	@method('PUT')


			         
			     


		           <div class="form-group">
	                <label class="col-md-3 control-label" for="demo-text-input">Autor</label>
	                    <div class="col-md-9">
	                        <select class="form-control" name="autor_id">
	                        	@forelse($autores as $autor)
	                        	
	                        		<option value="{{ $autor->id }}" @if($autor->id == $actualizar->autor_id) selected="" @endif>{{ $autor->sigi_nombres. ' '.$autor->sigi_apellidos }}</option>
	                        	@empty
	                        	<span class="text-danger">No se encontraron roles</span>

	                        	@endforelse
	                        </select>
	                        @error('autor_id')
	                        <small class="help-block text-danger">{{ $message }}</small>
	                        @enderror
	                    </div>
	            	 </div>	


			   
		             <div class="form-group">
		                <label class="col-md-3 control-label" for="demo-text-input">Seleccione Editorial</label>
		                    <div class="col-md-9">
		                        <select class="form-control" name="editorial_id">
		                        	@forelse($editoriales as $editorial)
		                        	
		                        		<option value="{{ $editorial->id }}" @if($editorial->id == $actualizar->editorial_id) selected="" @endif>{{ $editorial->sigi_nombre }}</option>


		                        	@empty
		                        	<span class="text-danger">No se encontraron roles</span>

		                        	@endforelse
		                        </select>
		                        @error('editorial_id')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		             </div>

			
			            <div class="form-group">
			                	<label class="col-md-3 control-label" for="demo-text-input">ISBN</label>
			                    <div class="col-md-9">
			                        <input type="text" required="" name="sigi_ISBN" id="isbn" onblur="run()" value="{{ isset($actualizar->sigi_ISBN) ? $actualizar->sigi_ISBN : '' }}" id="demo-text-input" class="form-control" placeholder="Ingrese ISBN">
			                        @error('sigi_ISBN')
			                        <small class="help-block text-danger">{{ $message }}</small>
			                        @enderror
			                    </div>
		                </div>
		
		             	

		                 <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-text-input">Titulo</label>
		                    <div class="col-md-9">
		                        <input type="text" id="titulo" name="sigi_titulo" value="{{ isset($actualizar->sigi_titulo) ? $actualizar->sigi_titulo : ''  }}"   id="demo-text-input" class="form-control" placeholder="Ingrese titulo">
		                        @error('sigi_titulo')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>

		                 <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-text-input">Año</label>
		                    <div class="col-md-9">
		                        <input type="text" id="anio" name="sigi_anio" value="{{ isset($actualizar->sigi_anio) ? $actualizar->sigi_anio : ''  }}"   id="demo-text-input" class="form-control" placeholder="Ingrese Año del libro">
		                        @error('sigi_anio')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>

		                 <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-text-input">Precio de venta</label>
		                    <div class="col-md-9">
		                        <input type="text" id="precio" name="sigi_precio_venta" value="{{ isset($actualizar->sigi_precio_venta) ? $actualizar->sigi_precio_venta : ''  }}"   id="demo-text-input" class="form-control" placeholder="Ingrese Precio de venta">
		                        @error('sigi_precio_venta')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>

		                   <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-text-input">Otros autores</label>
		                    <div class="col-md-9">
		                        <input  name="sigi_otros_autores"  style="resize: none" value="{{ isset($actualizar->sigi_otros_autores) ? $actualizar->sigi_otros_autores : ''  }}"   id="demo-text-input" class="form-control" placeholder="Ingrese otros autores">
		                        @error('sigi_otros_autores')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>

		                <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-email-input">Estado</label>
		                    <div class="col-md-9">
		                        <select name="sigi_estado" class="form-control">
		                        	<option>Seleccion de estado</option>	
		                        	<option value="A" @if($actualizar->sigi_estado == 'A') selected=""  @endif) >Activo</option>
		                        	<option value="I" @if($actualizar->sigi_estado == 'I') selected=""  @endif) >Inactivo</option>
		                        </select>
		                        @error('sigi_estado')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>
					
					              
					
					              
					
					              
					               
					 <div class="modal-footer">
	                    <a href="{{ route('libro.index') }}" class="btn btn-default" type="button">Close</a>
	                    <input type="submit" class="btn btn-primary"  onclick="return confirm('Desea Actualizar?')" value="Actualizar Libro" name="">
                    
               		 </div>
			            </form>
			            <!--===================================================-->
			            <!-- END BASIC FORM ELEMENTS -->
			
			
			        </div>
			    </div>

					   
		</div>
    


	
@endsection  
    
    


 @section('scripts')  
    
   
  


@endsection



