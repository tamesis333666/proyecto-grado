<nav id="mainnav-container">
                <div id="mainnav">


                  


                    <!--Menu-->
                    <!--================================-->
                    <div id="mainnav-menu-wrap">
                        <div class="nano">
                            <div class="nano-content">

                                <!--Profile Widget-->
                                <!--================================-->
                                <div id="mainnav-profile" class="mainnav-profile">
                                    <div class="profile-wrap text-center">
                                        <div class="pad-btm">
                                            <img class="img-circle img-md" src="{{ asset('admin/img/profile-photos/1.png') }}" alt="Profile Picture">
                                        </div>
                                        <a href="#profile-nav" class="box-block" data-toggle="collapse" aria-expanded="false">
                                            <span class="pull-right dropdown-toggle">
                                                <i class="dropdown-caret"></i>
                                            </span>

                                            <p class="mnp-name">@auth {{ Auth::user()->name }} @endauth</p>
                                            <span class="mnp-desc">@auth{{ Auth::user()->email }} @endauth</span>
                                        </a>
                                    </div>
                                    <div id="profile-nav" class="collapse list-group bg-trans">
                                       
                                       
                                        
                                        <a href="#" 
                                         href="{{ route('logout') }}"
                                         class="list-group-item"
                                         onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();"
                                         >
                                            <i class="demo-pli-unlock icon-lg icon-fw"></i> Logout
                                        </a>
                                      

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                </div>


                                <!--Shortcut buttons-->
                                <!--================================-->
                               
                                <!--================================-->
                                <!--End shortcut buttons-->


                                <ul id="mainnav-menu" class="list-group">
                        
                                    <!--Category name-->
                                    
                        
                        
                                  
                        
                                    <!--Category name-->
                                    <li class="list-header">Mantenimientos</li>
                            
                            @if(Auth::user()->role->sigi_nombre == 'ADMINISTRADOR')
                                    <!--Menu list item-->
                                    <li>
                                        <a href="#">
                                            <i class="demo-pli-add-user"></i>
                                            <span class="menu-title">Usuarios</span>
                                            <i class="arrow"></i>
                                        </a>
                        
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li><a href="{{ route('usuario.index') }}">Listado</a></li>
                                            
                                            <
                                            
                                        </ul>
                                    </li>
                                
                                    <!--Menu list item-->
                                    <li>
                                        <a href="#">
                                            <i class="demo-pli-computer-secure"></i>
                                            <span class="menu-title">Roles</span>
                                            <i class="arrow"></i>
                                        </a>
                        
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li><a href="{{ route('rol.index') }}">Listado</a></li>
                                            
                                            
                                        </ul>
                                    </li>
                            @endif
                                    <!--Menu list item-->
                                    <li>
                                        <a href="#">
                                            <i class="demo-pli-address-book"></i>
                                            <span class="menu-title">Editoriales</span>
                                            <i class="arrow"></i>
                                        </a>
                        
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li><a href="{{ route('editorial.index') }}">Listado</a></li>
                                        
                                            
                                        </ul>
                                    </li>
                        
                                    <!--Menu list item-->
                                    <li>
                                        <a href="#">
                                            <i class="demo-pli-file-txt"></i>
                                            <span class="menu-title">Libros</span>
                                            <i class="arrow"></i>
                                        </a>
                        
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li><a href="{{ route('libro.index') }}">Listado</a></li>
                                            
                                        </ul>
                                    </li>
                        
                                    <!--Menu list item-->
                                    <li>
                                        <a href="#">
                                            <i class="demo-pli-pen-5"></i>
                                            <span class="menu-title">Autores</span>
                                            <i class="arrow"></i>
                                        </a>
                        
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li><a href="{{ route('autor.index') }}">Listado</a></li>
                                            
                                            
                                        </ul>
                                    </li>
                        
                                    <!--Menu list item-->
                                    
                        
                                    <li class="list-divider"></li>
                        
                                   
                                                             
                                </ul>


                                <!--Widget-->
                                <!--================================-->
                               
                                <!--================================-->
                                <!--End widget-->

                            </div>
                        </div>
                    </div>
                    <!--================================-->
                    <!--End menu-->

                </div>
            </nav>