<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    protected $fillable=['autor_idÍndice','editorial_idÍndice','sigi_ISBN','sigi_titulo','sigi_anio','sigi_precio_venta','sigi_otros_autores','sigi_estado'];



    public function autor(){
        return $this->belongsTo('App\Autor');
    }

     public function editorial(){
    	return $this->belongsTo('App\Editorial');
    }


}
