<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    protected $fillable= ['sigi_nombres', 'sigi_apellidos','sigi_tipo_dni','sigi_dni','sigi_sexo','sigi_fecha_autor','sigi_email','sigi_estado'];
}
