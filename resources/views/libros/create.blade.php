@extends('layouts.plantilla')
@section('styles')


@endsection    
 
@section('content')
		
		<div class="row">
			    <div class="col-lg-12 ">
			        <div class="panel">
			            <div class="panel-heading">
			                <h3 class="panel-title">Agregar Informacion del libro</h3>
			            </div>
			
			
			       
					 <form action="{{ route('libro.store') }}" onsubmit="return validacionLibro()" method="post" class="panel-body form-horizontal form-padding">
                        @csrf
                        <!--Static-->
                        @include('libros.form',['mensaje'=> 'crear'])
                                    
                     </form>
			
			        </div>
			    </div>

					   
		</div>
    
		<!-- MODAL -->
		 <div class="modal fade" id="modal-isbn" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <!--Modal header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title text-center">Ejemplos de isbn </h4>
                </div>

                <!--Modal body-->
                <div class="modal-body">
                    <ul class="list-group">
					 

					  
					  <li class="list-group-item list-group-item-primary">99921-58-10-7</li>
					  <li class="list-group-item list-group-item-secondary">960 425 059 0</li>
					  <li class="list-group-item list-group-item-success">9780306406157</li>
					  <li class="list-group-item list-group-item-info">978-0-306-40615-7</li>	
					</ul>                   
                </div>

                <!--Modal footer-->
             
            </div>
        </div>
    </div>

	
@endsection  
    
    


 @section('scripts')  
    
   
  


@endsection



