<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
    <script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>
	 <script type="text/javascript">
           
         function validacionRol(){
         	var rol = document.getElementById('rol').value;
         	var expresion = /[A-Za-z ]+/;
         	if(rol === ""){
         		swal("Campo rol vacio  ", "click en ok para cerrar!", "warning");
         		return false;
         	}
         	
         	 else if(!expresion.test(rol)){
           	 	 swal("El rol no es valido", "click en ok para cerrar!", "warning");
                  
                      return false;
           	 }


         }
        </script>
</body>
</html>