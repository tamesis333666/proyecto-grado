@extends('layouts.plantilla')
@section('styles')
    <!--DataTables [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/plugins/datatables/extensions/Responsive/css/responsive.dataTables.min.css') }}" rel="stylesheet">
     <!--Summernote [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/summernote/summernote.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('admin/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    

@endsection 


 
@section('content')
        <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> 
                                <a  href="{{ route('libro.create') }}"  class="btn btn-warning btn-rounded " style="color: white">Agregar</a>

                                @auth 
                                @if(Auth::user()->role->sigi_nombre == 'ADMINISTRADOR') 
                                  <button data-target="#modal-aumentar" data-toggle="modal" class="btn btn-success btn-rounded">Porcentaje +</button>

                                  <button data-target="#modal-disminuir" data-toggle="modal" class="btn btn-danger btn-rounded">Porcentaje -</button>
                                @endif
                              @endauth
                            </h3>

                            
                             @if(Session::has('Mensaje'))
                                 <div class="panel-body demo-nifty-alert">
                                    
                                    <div class="row">
                                        <div class="col-sm-12">
                            
                                            <!-- Primary Alert -->
                                            <!--===================================================-->
                                            <div class="alert alert-primary">
                                                <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                                <strong>Mensaje!</strong>  {{ Session::get('Mensaje') }}
                                            </div>
                            
                                            <!-- Success Alert -->
                                            <!--===================================================-->
                                           
                            
                                        </div>
                                      
                                    </div>
                                 </div>
                                 
                               
                             @endif

                                    
                            

                              

                            
                        </div>
                        <div class="panel-body">
                            <table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Autor</th>  
                                        <th>Editorial</th>                                
                                        <th class="min-tablet">ISBN</th>
                                        <th class="min-desktop">Titulo</th>
                                        <th class="min-desktop">Año</th>
                                        <th class="min-desktop">Precio de venta</th>
                                         <th class="min-desktop">Otros autores</th>
                                        <th>Eliminar</th>
                                        <th>Editar</th>
                                        @auth @if(Auth::user()->role->sigi_nombre == 'ADMINISTRADOR')

                                         <th>Porcentaje</th>
                                        @endif
                                        @endauth
                                     
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($leer as $recorrer)
                                    <tr>
                                        <td> {{ isset($recorrer->autor->sigi_nombres) ? $recorrer->autor->sigi_nombres .' '. $recorrer->autor->sigi_apellidos   : '' }} 
                                        </td>
                                        <td>{{ isset($recorrer->editorial->sigi_nombre) ? $recorrer->editorial->sigi_nombre : '' }}
                                        </td>

                                        <td>{{ $recorrer->sigi_ISBN }}</td>
                                        <td>{{ $recorrer->sigi_titulo }}</td>
                                        <td>{{ $recorrer->sigi_anio }}</td>
                                        <td>{{ $recorrer->sigi_precio_venta }}</td>
                                        <td>{{ $recorrer->sigi_otros_autores }}</td>
                                        <td>
                                            <form action="{{ route('libro.destroy',$recorrer->id) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                  <button class="btn btn-success btn-icon" onclick="return confirm('Desea Eliminar?')"><i class="demo-psi-recycling icon-lg"></i></button>
                                            </form> 
                                            
                                        </td>
                                        <td>  
                                         

                                        
                                       
                                            <a class="btn btn-mint btn-icon" href="{{ route('libro.edit',$recorrer->id) }}"><i class="demo-psi-pen-5 icon-lg"></i></a>
                                        </td>
                                      
                                    @if(Auth::user()->role->sigi_nombre == 'ADMINISTRADOR')

                                        <td>
                                            <a href="{{ route('aumentap.edit',$recorrer->id) }}" class="btn btn-success btn-icon icon-lg fa fa-chevron-up">
                                                
                                            </a>
                                        @if($recorrer->sigi_precio_venta > 1)    
                                            <a href="{{ route('restap.edit',$recorrer->id) }}" class="btn btn-danger btn-icon icon-lg fa fa-chevron-down">
                                        @else
                                            <span class="text-danger">No disminuir precio</span>        
                                        @endif        
                                            </a>
                                        </td>
                                    @endif   
                                   
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
        </div>


    

        <!-- MODALES -->

  

    <div class="modal fade" id="modal-aumentar" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <!--Modal header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title text-center">Aumentar porcentaje a todos los Libros </h4>
                </div>

                <!--Modal body-->
                <div class="modal-body">
                     <form action="{{ route('porcentaje.aumentar') }}"  method="post" class="panel-body form-horizontal form-padding">
                        @csrf
                        <!--Static-->
                        @include('libros.formPorcentaje')
                                    
                     </form>
                   
                </div>

                <!--Modal footer-->
             
            </div>
        </div>
    </div>

     <div class="modal fade" id="modal-disminuir" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <!--Modal header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title text-center">Disminuir porcentaje a todos los Libros </h4>
                </div>

                <!--Modal body-->
                <div class="modal-body">
                     <form action="{{ route('porcentaje.disminuir') }}" onsubmit="return validacionPorcentaje()"  method="post" class="panel-body form-horizontal form-padding">
                        @csrf
                        <!--Static-->
                        @include('libros.formPorcentajeDisminuir')
                                    
                     </form>
                   
                </div>

                <!--Modal footer-->
             
            </div>
        </div>
    </div>

  

@endsection  
    
    
 

 @section('scripts')  
    
   
    <!--DataTables [ OPTIONAL ]-->
    <script src="{{ asset('admin/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>


    <!--DataTables Sample [ SAMPLE ]-->
    <script src="{{ asset('admin/js/demo/tables-datatables.js') }}"></script>

        <!--Summernote [ OPTIONAL ]-->
    <script src="{{ asset('admin/plugins/summernote/summernote.min.js') }}"></script>


    <!--Form File Upload [ SAMPLE ]-->
    <script src="{{ asset('admin/js/demo/form-text-editor.js') }}"></script>



@endsection



