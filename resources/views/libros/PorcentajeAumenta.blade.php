@extends('layouts.plantilla')
@section('styles')


@endsection    
 
@section('content')
		
		<div class="row">
			    <div class="col-lg-12 ">
			        <div class="panel">
			            <div class="panel-heading">
			                <h3 class="panel-title">Aumentar Porcentaje del libro {{ $actualizar->sigi_titulo }}
			                  Valor $ {{ $actualizar->sigi_precio_venta }}
			                </h3>

			            </div>
			
			
			       
			            <!--===================================================-->
			            <form  action="{{ route('aumentap.update', $actualizar->id) }}" onsubmit="return validacionPorcentaje()"  method="post" class="panel-body form-horizontal form-padding">
			            	@csrf
			            	@method('PUT')
			            	@include('libros.formPorcentajeIndividual',['mensaje'=>'aumentar'])
               		 </div>
			            </form>
			            <!--===================================================-->
			            <!-- END BASIC FORM ELEMENTS -->
			
			
			        </div>
			    </div>

					   
		</div>
    


	
@endsection  
    
    


 @section('scripts')  
    
   
  


@endsection



