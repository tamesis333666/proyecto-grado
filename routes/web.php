<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes([
	'register' => false,
	
]);

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/autor','AutorController')->except(['show']);
Route::resource('/editorial','EditorialController')->except(['show']);
Route::resource('/rol','RoleController')->except(['show']);
Route::resource('/usuario','UsuarioController')->except(['show']);
Route::resource('/libro','LibroController')->except(['show']);
Route::post('/porcentajea','PorcentajeController@aumentar')->name('porcentaje.aumentar');
Route::post('/porcentajed','PorcentajeController@disminuir')->name('porcentaje.disminuir');
Route::resource('aumentap','PorcentajeAumenta')->except(['create','store','destroy','show']);
Route::resource('restap','PorcentajeResta')->except(['create','store','destroy','show']);
