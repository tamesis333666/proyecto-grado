@extends('layouts.plantilla')
@section('styles')


@endsection    
 
@section('content')
@include("partials.validacionEditorial")		
		<div class="row">
			    <div class="col-lg-12 ">
			        <div class="panel">
			            <div class="panel-heading">
			                <h3 class="panel-title">Editar Informacion de la Editorial</h3>
			            </div>
			
			
			            <!-- BASIC FORM ELEMENTS -->
			            <!--===================================================-->
			            <form onsubmit="return validacionEditorial()"  action="{{ route('editorial.update', $actualizar->id) }}" method="post" class="panel-body form-horizontal form-padding">
			            	@csrf
			            	@method('PUT')
			
			                 					 <div class="form-group">
		                <label class="col-md-3 control-label" for="demo-text-input">Nombre Editorial</label>
		                    <div class="col-md-9">
		                        <input type="text" id="editorial" name="sigi_nombre" value="{{ isset($actualizar->sigi_nombre) ? $actualizar->sigi_nombre : '' }}" id="demo-text-input" class="form-control" placeholder="Ingrese nombre editorial">
		                        @error('sigi_nombre')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>
		
		             	

		                 <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-text-input">Email</label>
		                    <div class="col-md-9">
		                        <input type="text" id="email" name="sigi_email" value="{{ isset($actualizar->sigi_email) ? $actualizar->sigi_email : ''  }}"   id="demo-text-input" class="form-control" placeholder="Ingrese Email">
		                        @error('sigi_email')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>

		                <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-email-input">Estado</label>
		                    <div class="col-md-9">
		                        <select name="sigi_estado" class="form-control">
		                        	<option>Seleccion de estado</option>	
		                        	<option value="A" @if($actualizar->sigi_estado == 'A') selected=""  @endif) >Activo</option>
		                        	<option value="I" @if($actualizar->sigi_estado == 'I') selected=""  @endif) >Inactivo</option>
		                        </select>
		                        @error('sigi_estado')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>
					
					              
					
					              
					
					              
					               
					 <div class="modal-footer">
	                    <a href="{{ route('editorial.index') }}" class="btn btn-default" type="button">Close</a>
	                    <input type="submit" class="btn btn-primary"  onclick="return confirm('Desea Actualizar?')" value="Actualizar Editorial" name="">
                    
               		 </div>
			    </form>
			            <!--===================================================-->
			            <!-- END BASIC FORM ELEMENTS -->
			
			
			        </div>
			    </div>

					   
		</div>
    


	
@endsection  
    
    


 @section('scripts')  
    
   
  


@endsection



