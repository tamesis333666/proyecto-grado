<?php

namespace App\Http\Controllers;

use App\Libro;
use App\Editorial;
use App\Autor;
use Illuminate\Http\Request;

class LibroController extends Controller
{
    
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $libro = Libro::all();
        $leer = $libro;

        $autor = Autor::all();
        $autores = $autor;

        $editorial = Editorial::all();
        $editoriales = $editorial;
        return view('libros.index',compact('leer','autores','editoriales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
      $autor = Autor::all();
        $autores = $autor;

        $editorial = Editorial::all();
        $editoriales = $editorial;
        return view('libros.create',compact('autores','editoriales'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = [
            'autor_id' => 'required | ',
            'editorial_id' => 'required',
            'sigi_ISBN' => 'required | unique:libros',
            'sigi_titulo' => 'required | min:5 | max:20 | regex:/^[\pL\s\-]+$/u  |',
            'sigi_anio' => 'required | min: 4| max:4| alpha_num',
            'sigi_precio_venta' => 'required|max:22|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
            'sigi_otros_autores' => 'max:50|regex:/(^[A-Za-z0-9-, ]+$)+/'

        ];
      
      $this->validate($request,$datos);
        $datosLibro=request()->except('_token');

          $datosLibro['sigi_titulo'] = strtoupper($request->get('sigi_titulo'));
          $datosLibro['sigi_otros_autores'] = strtoupper($request->get('sigi_otros_autores'));

        
         
        Libro::insert($datosLibro);
       
        return redirect('libro')->with([
            'Mensaje' => 'Libro Agregado'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function show(Libro $libro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $autor = Autor::all();
        $autores = $autor;

        $editorial = Editorial::all();
        $editoriales = $editorial;

        $actualizar = Libro::findOrFail($id);
        return view('libros.edit',compact('actualizar','autores','editoriales'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
       
        $datos = [
            'autor_id' => 'required | ',
            'editorial_id' => 'required',
            'sigi_ISBN' => 'required ',
            'sigi_titulo' => 'required | min:5 | max:20 | regex:/^[\pL\s\-]+$/u  |',
            'sigi_anio' => 'required | min: 4| max:4| alpha_num',
            'sigi_precio_venta' => 'required|max:22|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
            'sigi_otros_autores' => 'max:50|regex:/(^[A-Za-z0-9-, ]+$)+/',
            'sigi_estado' => 'required'

        ];
      
      $this->validate($request,$datos);


       $datosLibro = request()->except('_token','_method'); 
       $datosLibro['sigi_titulo'] = strtoupper($request->get('sigi_titulo'));
       $datosLibro['sigi_otros_autores'] = strtoupper($request->get('sigi_otros_autores'));
        
       Libro::whereId($id)->update($datosLibro);
       
       return redirect("libro")->with([
        'Mensaje' => 'Libro actualizado'
       ]);    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Libro::whereId($id)->delete();
       
        return redirect('/libro')->with([
        'Mensaje' => 'Libro Eliminado',
         ]);
    }
}
