@include("partials.validacionPorcentaje")
		<div class="form-group">
       		 <label class="col-md-4 control-label" for="demo-text-input">Porcentaje(Ejemplo 10, 20 Ingrese de esta forma por favor)</label>
            <div class="col-md-8">
                <input type="text" required="" name="sigi_porcentaje"  id="porcentaje" onKeyPress="return soloNumeros(event)"  class="form-control" placeholder="Ingrese Procentaje">
                @error('sigi_porcentaje')
                <small class="help-block text-danger">{{ $message }}</small>
                @enderror
            </div>

             <div class="col-md-8">
                <input type="hidden" name="sigi_titulo"  value="{{ $actualizar->sigi_titulo }}" id="demo-text-input" class="form-control" placeholder="Ingrese Procentaje">
                @error('sigi_titulo')
                <small class="help-block text-danger">{{ $message }}</small>
                @enderror
            </div>

        </div>

         <div class="modal-footer">
            <a href="{{ route('libro.index') }}" class="btn btn-default" type="button">Close</a>
            <input type="submit" onclick="return confirm('Desea cambiar el porcentaje?')" class="btn btn-primary"  value="{{ $mensaje === 'aumentar' ? 'Aumentar ' : ' Disminuir' }} Porcentaje del libro" name="">
        
   	 	</div>