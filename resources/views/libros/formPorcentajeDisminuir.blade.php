@include("partials.validacionPorcentajeDisminuir")    
		<div class="form-group">
       		 <label class="col-md-4 control-label" for="demo-text-input">Porcentaje(Ejemplo 10, 20 Ingrese de esta forma por favor)</label>
            <div class="col-md-8">
                <input type="text" required="" name="sigi_porcentaje" onKeyPress="return soloNumeros(event)"  id="restarp" class="form-control" placeholder="Ingrese Procentaje">
                @error('sigi_porcentaje')
                <small class="help-block text-danger">{{ $message }}</small>
                @enderror
            </div>
        </div>

         <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <input type="submit" onclick="return confirm('Desea cambiar el porcentaje?')" class="btn btn-primary"  value=" Disminuir Porcentaje del libro" name="">
        
   		 </div>