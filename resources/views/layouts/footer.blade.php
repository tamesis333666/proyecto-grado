
    
    
    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--jQuery [ REQUIRED ]-->
    <script src="{{ asset('admin/js/jquery.min.js')}}"></script>


    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="{{ asset('admin/js/bootstrap.min.js')}}"></script>


    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="{{ asset('admin/js/nifty.min.js')}}"></script>




    <!--=================================================-->
    
    <!--Demo script [ DEMONSTRATION ]-->
    <script src="{{ asset('admin/js/demo/nifty-demo.min.js')}}"></script>

    
    


    <!--Specify page [ SAMPLE ]-->
  


    <script src="{{ asset('admin/js/demo/nifty-demo.min.js') }}"></script>
    <!--DataTables [ OPTIONAL ]-->
  


     @yield('scripts')

</body>
</html>
