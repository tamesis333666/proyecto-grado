<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
    <script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>
	 <script type="text/javascript">
           
      function validacionPorcentaje(){
        var restarp = document.getElementById('restarp').value;
        console.log(restarp);

        if(restarp === ""){
          swal("Campo porcentaje se encuentran vacio", "click en ok para cerrar!", "warning");
               
               return false;
        }
        else if(restarp < 1){
          swal("Porcentaje no correcto minimo 1 %", "click en ok para cerrar!", "warning");
               
               return false;
        }
        else if(restarp > 100){
          swal("Porcentaje no correcto maximo 100 %", "click en ok para cerrar!", "warning");
               
               return false;
        }
      }

     function soloNumeros(e){
          var key = window.Event ? e.which : e.keyCode
          return (key >= 48 && key <= 57)
      } 
    </script>
</body>
</html>