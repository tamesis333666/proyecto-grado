@extends('layouts.plantilla')
@section('styles')


@endsection    
 
@section('content')


		<div class="row">
			    <div class="col-lg-12 ">
			        <div class="panel">
			            <div class="panel-heading">
			                <h3 class="panel-title">Agregar Informacion del Autor</h3>
			            </div>
			
			
			            <!-- BASIC FORM ELEMENTS -->
			        <form action="{{ route('autor.store') }}" onsubmit="return esfechavalida() " method="post" name="forma" class="panel-body form-horizontal form-padding">
                        @csrf
                        <!--Static-->
                        @include('autores.form',['mensaje'=> 'crear'])
                                    
                     </form>
			            <!--===================================================-->
			            <!-- END BASIC FORM ELEMENTS -->
			
			
			        </div>
			    </div>

					   
		</div>
    


	
@endsection  
    
    


 @section('scripts')  
    
   
  


@endsection



