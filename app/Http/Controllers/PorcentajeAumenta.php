<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Libro;
use Illuminate\Support\Facades\DB;

class PorcentajeAumenta extends Controller
{
    

     public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
         return "Hola mundo";
    }

    
    public function edit($id)
    {
    

      
        $actualizar = Libro::findOrFail($id);
        return view('libros.PorcentajeAumenta',compact('actualizar'));
    }

  
    public function update(Request $request, $id)
    {
        $datos = [
            'sigi_porcentaje' => 'required | max:3'
        ];
        $this->validate($request,$datos);

        $porcentaje = $request->get('sigi_porcentaje');
        $datosPorcentaje  = $porcentaje/100;

        $titulo = $request->get('sigi_titulo');
        // return $datosPorcentaje;

        $consulta = DB::update('update libros set sigi_precio_venta = (sigi_precio_venta + (sigi_precio_venta*'.$datosPorcentaje.')) WHERE id = ?',[$id]);

        $porcentajeAumentado = $datosPorcentaje*100;
        return redirect('libro')->with([
            'Mensaje' => 'Se aumenton un '.$porcentajeAumentado. ' % al libro '.$titulo.' '
        ]); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
