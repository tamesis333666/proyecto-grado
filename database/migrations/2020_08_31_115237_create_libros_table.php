<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autors', function (Blueprint $table) {
            $table->id();
          
            $table->string('sigi_nombres')->nullable();
            $table->string('sigi_apellidos')->nullable();
            $table->string('sigi_tipo_dni')->nullable();
            $table->string('sigi_dni')->nullable();
            $table->string('sigi_sexo')->nullable();
            $table->date('sigi_fecha_autor')->nullable();
            $table->string('sigi_email')->nullable();
            $table->string('sigi_estado')->default('A');
            $table->timestamps();
        });

         Schema::create('editorials', function (Blueprint $table) {
            $table->id();
            $table->string('sigi_nombre')->nullable();
            $table->string('sigi_email')->nullable();
            $table->string('sigi_estado')->default('A');

          
            $table->timestamps();
        });
         
        Schema::create('libros', function (Blueprint $table) {
            $table->id();
            $table->foreignId('autor_id')->references('id')->on('Autors')->nullable();
            $table->foreignId('editorial_id')->references('id')->on('Editorials')->nullable();
            $table->string('sigi_ISBN')->nullable();
            $table->string('sigi_titulo')->nullable();
            $table->string('sigi_anio')->nullable();
            $table->decimal('sigi_precio_venta',11,2)->nullable();
            $table->text('sigi_otros_autores')->nullable();
            $table->string('sigi_estado')->default('A');
            $table->timestamps();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libros');
         Schema::dropIfExists('Autors');
          Schema::dropIfExists('Editorials');
    }
}
