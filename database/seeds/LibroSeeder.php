<?php

use Illuminate\Database\Seeder;
use App\Libro;

class LibroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('Libros')->insert([
       		'autor_id' => '1',
       		'editorial_id' => '2',
       		'sigi_ISBN' => '978-2403-577-0',
       		'sigi_titulo' => strtoupper('La pradera'),
       		'sigi_anio' => '1998',
       		'sigi_precio_venta' => 44.50,
       		'sigi_otros_autores' => strtoupper('Luis fernando, Patrin lomas'),
       		


       ]);

        DB::table('Libros')->insert([
       		'autor_id' => '2',
       		'editorial_id' => '3',
       		'sigi_ISBN' => '978-84-613-0053-2',
       		'sigi_titulo' => strtoupper('Alijab'),
       		'sigi_anio' => '1994',
       		'sigi_precio_venta' => 54.30,
       		'sigi_otros_autores' => strtoupper('Victor Andrade'),
       		


       ]);

         DB::table('Libros')->insert([
       		'autor_id' => '1',
       		'editorial_id' => '2',
       		'sigi_ISBN' => '978-612-466241-9-2',
       		'sigi_titulo' => strtoupper('El principito'),
       		'sigi_anio' => '2000',
       		'sigi_precio_venta' => 20.10,
       		'sigi_otros_autores' => strtoupper('Maria Sanchez'),
       		


       ]);


    }
}
