@extends('layouts.plantilla')
@section('styles')
    <!--DataTables [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/plugins/datatables/extensions/Responsive/css/responsive.dataTables.min.css') }}" rel="stylesheet">

    

@endsection    

 
@section('content')

        <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> 
                                <a href="{{ route('usuario.create') }}" class="btn btn-warning btn-rounded" style="color:white;">Agregar</a>
                            </h3>
                            
                           @if(Session::has('Mensaje'))
                             <div class="panel-body demo-nifty-alert">
                                
                                <div class="row">
                                    <div class="col-sm-12">
                        
                                        <!-- Primary Alert -->
                                        <!--===================================================-->
                                        <div class="alert alert-primary">
                                            <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                            <strong>Mensaje!</strong>  {{ Session::get('Mensaje') }}
                                        </div>
                        
                                        <!-- Success Alert -->
                                        <!--===================================================-->
                                       
                        
                                    </div>
                                  
                                </div>
                              </div>
                               
                             @endif

                        </div>
                        <div class="panel-body">
                            <table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Rol</th>                                 
                                        <th class="min-tablet">Nombres</th>
                                        <th class="min-desktop">Email</th>
                                        <th>Dni</th>
                                        <th>Sexo</th>
                                        <th>Direccion</th>
                                        <th>Telefono</th>
                                        <th>Estado</th>
                                        <th>Foto</th>
                                        <th>Eliminar</th>
                                        <th>Editar</th>
                                     
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($leer as $recorrer)
                                    <tr>
                                        <td>{{ isset($recorrer->role->sigi_nombre) ? $recorrer->role->sigi_nombre : '' }}</td>
                                        <td>{{ $recorrer->name }}</td>
                                        <td>{{ $recorrer->email }}</td>
                                        <td>{{ $recorrer->dni }}</td>
                                        <td>{{ $recorrer->sexo }}</td>
                                        <td>{{ $recorrer->direccion }}</td>
                                        <td>{{ $recorrer->telefono }}</td>
                                        <td>{{ $recorrer->estado }}</td>
                                        <td><img src="{{ asset('storage'.'/'.$recorrer->foto) }}" width="50" class="img-thumbnail"></td>
                                        <td>
                                            <form action="{{ route('usuario.destroy',$recorrer->id) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                  <button class="btn btn-success btn-icon" onclick="return confirm('Desea Eliminar?')"><i class="demo-psi-recycling icon-lg"></i></button>
                                            </form> 
                                            
                                        </td>
                                        <td>  
                                         

                                        
                                       
                                            <a class="btn btn-mint btn-icon" href="{{ route('usuario.edit',$recorrer->id) }}"><i class="demo-psi-pen-5 icon-lg"></i></a>
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
        </div>


    

        <!-- MODALES -->

    
@endsection  
    
    
 

 @section('scripts')  
    
   
    <!--DataTables [ OPTIONAL ]-->
    <script src="{{ asset('admin/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>


    <!--DataTables Sample [ SAMPLE ]-->
    <script src="{{ asset('admin/js/demo/tables-datatables.js') }}"></script>


@endsection



