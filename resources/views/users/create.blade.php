@extends('layouts.plantilla')
@section('styles')


@endsection    
 
@section('content')
		
		<div class="row">
			    <div class="col-lg-12 ">
			        <div class="panel">
			            <div class="panel-heading">
			                <h3 class="panel-title">Agregar Informacion del usuario</h3>
			            </div>
			
			
			            <!-- BASIC FORM ELEMENTS -->
			            <!--===================================================-->
			      	 <form action="{{ route('usuario.store') }}" onsubmit="return esfechavalida()" method="post" enctype="multipart/form-data" class="panel-body form-horizontal form-padding">
                        @csrf
                        <!--Static-->
                        @include('users.form',['mensaje'=> 'crear'])
                                    
                     </form>
			            <!--===================================================-->
			            <!-- END BASIC FORM ELEMENTS -->
			
			
			        </div>
			    </div>

					   
		</div>
    


	
@endsection  
    
    


 @section('scripts')  
    
   
  


@endsection



