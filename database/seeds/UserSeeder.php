<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
   

    public function run()
    {
       DB::table('Users')->insert([
       		'role_id' => '1',
       		'name' =>  strtoupper('Luis Lomas'),
       		'email' => strtoupper('admin@gmail.com'),
       		'password' => bcrypt('12345678'),
       		'dni' => '1725024283',
       		'sexo' => strtoupper('H'),
       		'direccion' => strtoupper('Calderon y avenida panamerica OE40'),
          'telefono' => '0984272711',
       		'fecha_nacimiento' => '1996-04-05',


       ]);

        DB::table('Users')->insert([
       		'role_id' => '2',
       		'name' => strtoupper('juan Perez'),
       		'email' => strtoupper('juan@gmail.com'),
       		'password' => bcrypt('12345678'),
       		'dni' => '10024041655',
       		'sexo' => 'H',
       		'direccion' => strtoupper('Amazonas  y shiris OE50'),
          'telefono' => '0987140699',
       		'fecha_nacimiento' => '1991-04-05',
       	

       ]);
    }
}
