<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
    <script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>
	 <script type="text/javascript">	 	 
           	 	 	 	 	
         

        function esfechavalida() {
      var fecha = document.forma.fecha.value;


      // La longitud de la fecha debe tener exactamente 10 caracteres
      if ( fecha.length !== 10 )
         error = true;

      // Primero verifica el patron
      if ( !/^\d{4}\/\d{1,2}\/\d{1,2}$/.test(fecha) )
         error = true;

      // Mediante el delimitador "/" separa dia, mes y año
    var fecha = fecha.split("-");
     var year = parseInt(fecha[0]);
     if(year < 1800){
      swal("Fecha Inválida: * La Fecha minima es 1800.\n", "click en ok para cerrar!", "warning");
        return false;
     }
      if(year > 2005){
      swal("Fecha Inválida: * La Fecha maxima es 2005.\n", "click en ok para cerrar!", "warning");
        return false;
     }
     var month = parseInt(fecha[1]);
      
      var day = parseInt(fecha[2]);

      // Verifica que dia, mes, año, solo sean numeros
      error = ( isNaN(day) || isNaN(month) || isNaN(year) );

      // Lista de dias en los meses, por defecto no es año bisiesto
      var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
      if ( month === 1 || month > 2 )
         if ( day > ListofDays[month-1] || day < 0 || ListofDays[month-1] === undefined )
            error = true;

      // Detecta si es año bisiesto y asigna a febrero 29 dias
      if ( month === 2 ) {
         var lyear = ( (!(year % 4) && year % 100) || !(year % 400) );
         if ( lyear === false && day >= 29 )
            error = true;
         if ( lyear === true && day > 29 )
            error = true;
      }

      if ( error === true ) {
        swal("Fecha Inválida: * La Fecha debe tener el formato: aaaa-mm-dd (año-mes-dia).\n", "click en ok para cerrar!", "warning");
        return false;
      }
      else
         alert("Fecha Válida\n");

      return true;
   }

       function soloNumeros(e){
          var key = window.Event ? e.which : e.keyCode
          return (key >= 48 && key <= 57)
      } 
    
           

        </script>
</body>
</html>