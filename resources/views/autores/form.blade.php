@include("partials.validacion")
@include("partials.validacionAutor")


  					 <div class="form-group">
		                <label class="col-md-3 control-label" for="demo-text-input">Nombres</label>
		                    <div class="col-md-9">
		                        <input type="text"  id="nombres" value="{{ old('sigi_nombres') }}" name="sigi_nombres" value="{{ isset($actualizar->sigi_nombres) ? $actualizar->sigi_nombres : '' }}" id="demo-text-input" class="form-control" placeholder="Nombres">
		                        @error('sigi_nombres')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>
		
		                <!--Text Input-->
		                <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-text-input">Apellidos</label>
		                    <div class="col-md-9">
		                        <input type="text"  id="apellidos" value="{{ old('sigi_apellidos') }}" name="sigi_apellidos" value="{{ isset($actualizar->sigi_apellidos) ? $actualizar->sigi_apellidos : ''  }}" id="demo-text-input" class="form-control" placeholder="Apellidos">
		                        @error('sigi_apellidos')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>
		
		                <!--Email Input-->
		                <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-email-input">Tipo DNI</label>
		                    <div class="col-md-9">
		                        <select required="" name="sigi_tipo_dni" class="form-control">
		                        	<option value="">Seleccion tipo de dni</option>	
		                        	<option value="C" >Cedula Ciudadania</option>
		                        	<option value="P" >Pasaporte</option>
		                        </select>

		                        @error('sigi_tipo_dni')
									<span class="invalid-feedback d-block" role="alert">
										{{ $message }}
									</span>
									
								@enderror	
		                        
		                    </div>
		                </div>

		                 <div class="form-group">
		                    <label class="col-md-3  control-label" for="demo-text-input">Dni</label>
		                    <div class="col-md-9">
		                        <input type="number" value="{{ old('sigi_dni') }}" required="" id="documento" onblur="validarDocumento()" name="sigi_dni"  onKeyPress="return soloNumeros(event)" value="{{ isset($actualizar->sigi_dni) ? $actualizar->sigi_dni : ''  }}" id="demo-text-input" class="form-control" placeholder="Ingrese Dni">
		                         @error('sigi_dni')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>

		                <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-email-input">Sexo</label>
		                    <div class="col-md-9">
		                        <select required="" name="sigi_sexo" class="form-control">
		                        	<option value="">Seleccion tipo de sexo</option>	
		                        	<option value="H">Hombre</option>
		                        	<option value="M">Mujer</option>
		                        </select>

	                           @error('sigi_sexo')
								<span class="invalid-feedback d-block" role="alert">
									{{ $message }}
								</span>
								@enderror	
		                       
		                    </div>
		                </div>

		                 <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-text-input">Fecha Nacimiento</label>
		                    <div class="col-md-9">
		                        <input type="text"  id="fecha" value="{{ old('sigi_fecha_autor') }}"  name="sigi_fecha_autor" value="{{ isset($actualizar->sigi_fecha_autor) ? $actualizar->sigi_fecha_autor : ''  }}"  id="demo-text-input" class="form-control" placeholder="Ingrese fecha">
		                        @error('sigi_fecha_autor')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>
		

		                 <div class="form-group">
		                    <label class="col-md-3 control-label" for="demo-text-input">Email</label>
		                    <div class="col-md-9">
		                        <input type="text"  id="email" value="{{ old('sigi_email') }}" name="sigi_email" value="{{ isset($actualizar->sigi_email) ? $actualizar->sigi_email : ''  }}"   id="demo-text-input" class="form-control" placeholder="Ingrese Email">
		                        @error('sigi_email')
		                        <small class="help-block text-danger">{{ $message }}</small>
		                        @enderror
		                    </div>
		                </div>         
								              
								              
					               
					<div class="modal-footer">
	                    <a href="{{ route('autor.index') }}" class="btn btn-default" type="button">Close</a>
	                    <input type="submit"  class="btn btn-primary"  onclick="return confirm('Desea Agregar?')" value="Crear Actor" name="">
                    
               		 </div>

               		

               		