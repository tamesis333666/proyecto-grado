<?php

use Illuminate\Database\Seeder;
use App\Editorial;

class EditorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Editorial::class,14)->create();
    }
}
