@extends('layouts.plantilla')
@section('styles')
    <!--DataTables [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/plugins/datatables/extensions/Responsive/css/responsive.dataTables.min.css') }}" rel="stylesheet">

    

@endsection    
 


@section('content')
        <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> 
                                <a href="{{ route('autor.create') }}" style="color:white;" class="btn btn-warning btn-rounded">Agregar</a>

                            </h3>
                            @if(Session::has('Mensaje'))
                                 <div class="panel-body demo-nifty-alert">
                                    
                                    <div class="row">
                                        <div class="col-sm-12">
                            
                                            <!-- Primary Alert -->
                                            <!--===================================================-->
                                            <div class="alert alert-primary">
                                                <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                                <strong>Mensaje!</strong>  {{ Session::get('Mensaje') }}
                                            </div>
                            
                                            <!-- Success Alert -->
                                            <!--===================================================-->
                                           
                            
                                        </div>
                                      
                                    </div>
                                 </div>
                                   
                             @endif
                        </div>
                        <div class="panel-body">
                            <table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nombres - Apellidos</th>                                 
                                        <th class="min-tablet">DNI.</th>
                                        <th class="min-desktop">Sexo</th>
                                        <th class="min-desktop">Fecha Nacim.</th>
                                        <th class="min-desktop">Email</th>
                                        <th class="min-desktop">Estado</th>
                                        <th>Eliminar</th>
                                        <th>Editar</th>
                                     
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($leer as $recorrer)
                                    <tr>
                                        <td>{{ $recorrer->sigi_nombres }} {{ $recorrer->sigi_apellidos }}</td>                               
                                        <td>{{ $recorrer->sigi_dni }}</td>
                                        <td>{{ $recorrer->sigi_sexo }}</td>
                                        <td>{{ $recorrer->sigi_fecha_autor }}</td>
                                        <td>{{ $recorrer->sigi_email }}</td>
                                        <td>{{ $recorrer->sigi_estado }}</td>
                                        <td>
                                            <form action="{{ route('autor.destroy',$recorrer->id) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                  <button class="btn btn-success btn-icon" onclick="return confirm('Desea Eliminar?')"><i class="demo-psi-recycling icon-lg"></i></button>
                                            </form> 
                                            
                                        </td>
                                        <td>  
                                         

                                        
                                       
                                            <a class="btn btn-mint btn-icon" href="{{ route('autor.edit',$recorrer->id) }}" ><i class="demo-psi-pen-5 icon-lg"></i></a>
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
        </div>


    

        <!-- MODALES -->

    <div class="modal fade" id="demo-default-modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <!--Modal header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title text-center">Ingresar Autor </h4>
                </div>

                <!--Modal body-->
                <div class="modal-body">
                     <form action="{{ route('autor.store') }}" method="post" name="forma" class="panel-body form-horizontal form-padding">
                        @csrf
                        <!--Static-->
                        @include('autores.form',['mensaje'=> 'crear'])
                                    
                     </form>
                   
                </div>

                <!--Modal footer-->
             
            </div>
        </div>
    </div>
@endsection  
    
    
 

 @section('scripts')  
    
   
    <!--DataTables [ OPTIONAL ]-->
    <script src="{{ asset('admin/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>


    <!--DataTables Sample [ SAMPLE ]-->
    <script src="{{ asset('admin/js/demo/tables-datatables.js') }}"></script>


@endsection



