@extends('layouts.plantilla')
@section('styles')
    <!--DataTables [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/plugins/datatables/extensions/Responsive/css/responsive.dataTables.min.css') }}" rel="stylesheet">

    

@endsection    
 


@section('content')
         <div class="row">
                            
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-sm-4 col-lg-4">
                    
                                        <!--Sparkline Area Chart-->
                                        <div class="panel panel-success panel-colorful">
                                            <div class="pad-all">
                                                <p class="text-lg text-semibold"><i class="demo-pli-data-storage icon-fw"></i>Usuarios</p>
                                                <p class="mar-no">
                                                    <span class="pull-right text-bold">{{ $usuarios }}</span> Total
                                                </p>
                                                
                                            </div>
                                            <div class="pad-top text-center">
                                                <!--Placeholder-->
                                                <div id="demo-sparkline-area" class="sparklines-full-content"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-4">
                    
                                        <!--Sparkline Line Chart-->
                                        <div class="panel panel-info panel-colorful">
                                            <div class="pad-all">
                                                <p class="text-lg text-semibold">Roles</p>
                                                <p class="mar-no">
                                                    <span class="pull-right text-bold">{{ $roles }}</span> Total
                                                </p>
                                                
                                            </div>
                                            <div class="pad-top text-center">
                    
                                                <!--Placeholder-->
                                                <div id="demo-sparkline-line" class="sparklines-full-content"></div>
                    
                                            </div>
                                        </div>
                                    </div>

                                     <div class="col-sm-4 col-lg-4">
                    
                                        <!--Sparkline Line Chart-->
                                        <div class="panel panel-warning panel-colorful">
                                            <div class="pad-all">
                                                <p class="text-lg text-semibold">Editoriales</p>
                                                <p class="mar-no">
                                                    <span class="pull-right text-bold">{{ $editoriales }}</span> Total
                                                </p>
                                                
                                            </div>
                                            <div class="pad-top text-center">
                    
                                                <!--Placeholder-->
                                                <div id="demo-sparkline-line" class="sparklines-full-content"></div>
                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-lg-4">
                    
                                        <!--Sparkline bar chart -->
                                        <div class="panel panel-purple panel-colorful">
                                            <div class="pad-all">
                                                <p class="text-lg text-semibold"><i class="demo-pli-basket-coins icon-fw"></i> Libros</p>
                                                <p class="mar-no">
                                                    <span class="pull-right text-bold">{{ $libros }}</span> 
                                                    Total
                                                </p>
                                                
                                            </div>
                                            <div class="text-center">
                    
                                                <!--Placeholder-->
                                                <div id="demo-sparkline-bar" class="box-inline"></div>
                    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-4">
                    
                                        <!--Sparkline pie chart -->
                                        <div class="panel panel-danger panel-colorful">
                                            <div class="pad-all">
                                                <p class="text-lg text-semibold">Autores</p>
                                                <p class="mar-no">
                                                    <span class="pull-right text-bold">{{ $autores }}</span> Total
                                                </p>
                                              
                                            </div>
                                             <div class="text-center">
                    
                                                <!--Placeholder-->
                                                <div id="demo-sparkline-bar" class="box-inline"></div>
                    
                                            </div>
                                         
                                        </div>
                                    </div>
                                </div>
                    
                    
                                <!--Extra Small Weather Widget-->
                                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            
                    
                                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                                <!--End Extra Small Weather Widget-->
                    
                    
                            </div>
        </div>
@endsection  
    
    
 

 @section('scripts')  
    
   
    <!--DataTables [ OPTIONAL ]-->
    <script src="{{ asset('admin/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>


    <!--DataTables Sample [ SAMPLE ]-->
    <script src="{{ asset('admin/js/demo/tables-datatables.js') }}"></script>


@endsection



