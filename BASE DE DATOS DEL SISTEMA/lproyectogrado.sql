-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-09-2020 a las 18:12:49
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lproyectogrado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autors`
--

CREATE TABLE `autors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sigi_nombres` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sigi_apellidos` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sigi_tipo_dni` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sigi_dni` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sigi_sexo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sigi_fecha_autor` date DEFAULT NULL,
  `sigi_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sigi_estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `autors`
--

INSERT INTO `autors` (`id`, `sigi_nombres`, `sigi_apellidos`, `sigi_tipo_dni`, `sigi_dni`, `sigi_sexo`, `sigi_fecha_autor`, `sigi_email`, `sigi_estado`, `created_at`, `updated_at`) VALUES
(1, 'D\'ANGELO', 'BEATTY', 'C', '0420662416', 'H', '1976-01-02', 'USCHADEN@HOTMAIL.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(2, 'HARMON', 'O\'REILLY', 'C', '3712427636', 'M', '1971-07-11', 'EMMANUEL.MCCLURE@GMAIL.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(3, 'GAETANO', 'WATSICA', 'C', '5220368081', 'H', '1998-10-31', 'ERWIN26@YAHOO.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(4, 'CHANCE', 'LANGWORTH', 'P', '9983476545', 'H', '2008-07-21', 'ANGELO.UPTON@GMAIL.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(5, 'IAN', 'KUHLMAN', 'P', '8116835756', 'M', '2018-11-08', 'BRENDAN38@BOYER.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(6, 'BRYCE', 'HAUCK', 'P', '3555231221', 'M', '1977-06-12', 'NOBLE90@GMAIL.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(7, 'JAN', 'GERHOLD', 'C', '6790786273', 'H', '2012-09-08', 'HAZLE.VONRUEDEN@KULAS.NET', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(8, 'OKEY', 'KERLUKE', 'C', '8070836807', 'H', '2013-06-14', 'BREANNA.SCHROEDER@YAHOO.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(9, 'ABDULLAH', 'HARBER', 'P', '8862171503', 'H', '2016-11-15', 'WSCHULIST@HOTMAIL.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(10, 'GUIDO', 'MAYER', 'P', '6503693194', 'M', '2011-07-01', 'TIFFANY50@YAHOO.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(11, 'JOANY', 'KUNDE', 'C', '4187604305', 'H', '1989-08-25', 'MBOGISICH@HOTMAIL.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(12, 'CONOR', 'KASSULKE', 'C', '7046214227', 'H', '2002-04-05', 'JAIRO09@HOTMAIL.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(13, 'ZACK', 'ZBONCAK', 'P', '4111683496', 'H', '2005-10-05', 'ANKUNDING.YESENIA@HOTMAIL.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(14, 'SOLON', 'GLEICHNER', 'P', '0258992184', 'M', '1999-09-17', 'JACOBI.CLETA@GMAIL.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `editorials`
--

CREATE TABLE `editorials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sigi_nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sigi_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sigi_estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `editorials`
--

INSERT INTO `editorials` (`id`, `sigi_nombre`, `sigi_email`, `sigi_estado`, `created_at`, `updated_at`) VALUES
(1, 'HOPPE, KOEPP AND GREEN', 'WISOZK.ALANIS@EXAMPLE.ORG', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(2, 'LOWE, BERGNAUM AND WALSH', 'RYLAN.NICOLAS@EXAMPLE.NET', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(3, 'MCGLYNN-OSINSKI', 'JADYN.GREENFELDER@EXAMPLE.NET', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(4, 'NADER, LUETTGEN AND DUBUQUE', 'CAROLYN31@EXAMPLE.NET', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(5, 'REYNOLDS-LIND', 'GORCZANY.MERVIN@EXAMPLE.NET', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(6, 'RITCHIE, THIEL AND ERNSER', 'KLING.GARTH@EXAMPLE.ORG', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(7, 'SPENCER-EFFERTZ', 'XMARKS@EXAMPLE.ORG', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(8, 'HEATHCOTE AND SONS', 'EDEN97@EXAMPLE.ORG', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(9, 'TURNER-ROOB', 'EDYTH88@EXAMPLE.ORG', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(10, 'BREITENBERG-REICHEL', 'TILLMAN.NOVA@EXAMPLE.ORG', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(11, 'GAYLORD, ZIEMANN AND SCHOEN', 'EWEST@EXAMPLE.NET', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(12, 'GOYETTE-JACOBS', 'QWILLMS@EXAMPLE.NET', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(13, 'MOORE, O\'HARA AND YOST', 'LINNEA60@EXAMPLE.ORG', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53'),
(14, 'YOST-KONOPELSKI', 'CARTER20@EXAMPLE.COM', 'A', '2020-09-14 08:04:53', '2020-09-14 08:04:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libros`
--

CREATE TABLE `libros` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `autor_id` bigint(20) UNSIGNED NOT NULL,
  `editorial_id` bigint(20) UNSIGNED NOT NULL,
  `sigi_ISBN` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sigi_titulo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sigi_anio` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sigi_precio_venta` decimal(11,2) DEFAULT NULL,
  `sigi_otros_autores` text COLLATE utf8mb4_unicode_ci,
  `sigi_estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `libros`
--

INSERT INTO `libros` (`id`, `autor_id`, `editorial_id`, `sigi_ISBN`, `sigi_titulo`, `sigi_anio`, `sigi_precio_venta`, `sigi_otros_autores`, `sigi_estado`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '978-2403-577-0', 'LA PRADERA', '1998', '101.85', 'LUIS FERNANDO, PATRIN LOMAS', 'A', NULL, NULL),
(2, 2, 3, '978-84-613-0053-2', 'ALIJAB', '1994', '125.52', 'VICTOR ANDRADE', 'A', NULL, NULL),
(3, 1, 2, '978-612-466241-9-2', 'EL PRINCIPITO', '2000', '46.47', 'MARIA SANCHEZ', 'A', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(51, '2014_10_12_000000_create_users_table', 1),
(52, '2014_10_12_100000_create_password_resets_table', 1),
(53, '2019_08_19_000000_create_failed_jobs_table', 1),
(54, '2020_08_31_115237_create_libros_table', 1),
(55, '2020_09_04_134044_create_roles_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sigi_nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sigi_estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `sigi_nombre`, `sigi_estado`, `created_at`, `updated_at`) VALUES
(1, 'ADMINISTRADOR', 'A', NULL, NULL),
(2, 'VENDEDOR', 'A', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `password`, `dni`, `sexo`, `direccion`, `telefono`, `fecha_nacimiento`, `foto`, `estado`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'LUIS LOMAS', 'ADMIN@GMAIL.COM', NULL, '$2y$10$YF5i7sPcoKHdxnxjMFmMLO7mG4jLExu0Wf8odqv.ApOTVDjpuMQQe', '1725024283', 'H', 'CALDERON Y AVENIDA PANAMERICA OE40', '0984272711', '1996-04-05', NULL, 'A', NULL, NULL, NULL),
(2, 2, 'JUAN PEREZ', 'JUAN@GMAIL.COM', NULL, '$2y$10$QaRzS7ni/SaaNcUxWoBnFu9FarBYVXUSbByelByFFcfa35M10DSIm', '10024041655', 'H', 'AMAZONAS  Y SHIRIS OE50', '0987140699', '1991-04-05', NULL, 'A', NULL, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `autors`
--
ALTER TABLE `autors`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `editorials`
--
ALTER TABLE `editorials`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `libros`
--
ALTER TABLE `libros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `libros_autor_id_foreign` (`autor_id`),
  ADD KEY `libros_editorial_id_foreign` (`editorial_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `autors`
--
ALTER TABLE `autors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `editorials`
--
ALTER TABLE `editorials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `libros`
--
ALTER TABLE `libros`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `libros`
--
ALTER TABLE `libros`
  ADD CONSTRAINT `libros_autor_id_foreign` FOREIGN KEY (`autor_id`) REFERENCES `autors` (`id`),
  ADD CONSTRAINT `libros_editorial_id_foreign` FOREIGN KEY (`editorial_id`) REFERENCES `editorials` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
