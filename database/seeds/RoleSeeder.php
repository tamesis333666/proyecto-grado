<?php

use Illuminate\Database\Seeder;
use App\Role;
use Illuminate\Support\Facades\DB;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('Roles')->insert([
            'sigi_nombre' => strtoupper("Administrador"),
          
        ]);

        DB::table('Roles')->insert([
            'sigi_nombre' => strtoupper("Vendedor"),
          
        ]);
    }
}
